const { Client } = require('pg')
const db = require('../config/database');
const connectionString = db.databaseURI;


const client = new Client({
  connectionString: connectionString,
})

client.connect()

// create the tables
  // create the cashier table;
const cashier = `CREATE TABLE Cashier (
                  CashierID SERIAL PRIMARY KEY,
                  FirstName varchar(255),
                  LastName varchar(255),
                  Email varchar(255));`
  client.query(cashier, (err, res) => {
  console.log(err, res)
  client.end();
})


