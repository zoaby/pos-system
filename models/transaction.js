const { Client } = require('pg')
const db = require('../config/database');
const connectionString = db.databaseURI;


const client = new Client({
  connectionString: connectionString,
})

client.connect()


// create the transaction table;
const transaction = `CREATE TABLE Transaction (
    TransactionID SERIAL PRIMARY KEY,
    Time timestamp default current_timestamp,
    Cost int,
    paid int,
    Change int,
    CashierID int REFERENCES Cashier (CashierID)
  );`


client.query(transaction, (err, res) => {
console.log(err, res)
client.end()
})