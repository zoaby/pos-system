const express = require('express');
const router = express.Router();
const helpers = require('../helpers/cashiersHelpers');

/* GET chashiers listing. */
router.get('/', helpers.getCashiers);

// GET cashier by id;
router.get('/:id', helpers.getCashierById);

//  retrive user info ;
router.post('/login', helpers.getCashierByEmail);

// add new cashier
router.post('/add', helpers.createNewCashier);

// delete one cashier;
router.delete('/:id', helpers.deleteCashier);




module.exports = router;
