const express = require('express');
const router = express.Router();
const helpers = require('../helpers/transactonsHelpers');


// retrieve all the transactions;
router.get('/', helpers.getTransactions);

// get all transactions for one cashier;
router.get('/:id', helpers.getTransactionsForCashier);

// create new transaction;
router.post('/add', helpers.createNewTransaction);

module.exports = router;
