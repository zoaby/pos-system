import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LogsComponent } from './logs/logs.component';
import { AddComponent } from './add/add.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RegCashierComponent } from './reg-cashier/reg-cashier.component';
import { AuthService } from './services/auth/auth.service';
import { TransactionsService } from './services/transactions/transactions.service';

// The Routes;
const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'add', component: AddComponent },
  { path: 'logs', component: LogsComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegCashierComponent },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LogsComponent,
    AddComponent,
    LoginComponent,
    RegCashierComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatInputModule,
    FormsModule,
    MatSelectModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    TransactionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
