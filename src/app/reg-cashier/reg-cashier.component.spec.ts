import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegCashierComponent } from './reg-cashier.component';

describe('RegCashierComponent', () => {
  let component: RegCashierComponent;
  let fixture: ComponentFixture<RegCashierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegCashierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegCashierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
