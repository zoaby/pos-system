import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg-cashier',
  templateUrl: './reg-cashier.component.html',
  styleUrls: ['./reg-cashier.component.css']
})
export class RegCashierComponent implements OnInit {
  done = false;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {}

  // when create new cashier
  onRegister(form: NgForm) {
    this.authService.registerCashier(form.value)
    .subscribe(res => {
      this.done = true;
      setTimeout(() => {
        this.done = false;
        this.router.navigate(['/login']);
      }, 4000);
    }, err => {
      console.log(err);
    });
  }
}
