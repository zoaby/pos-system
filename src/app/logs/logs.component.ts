import { Component, OnInit } from '@angular/core';
import { Cashier } from '../services/auth/cashier';
import { TransactionsService } from '../services/transactions/transactions.service';


@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {
  cashiers: any;
  allTransactions: any;
  constructor(private transactions: TransactionsService) { }

  ngOnInit() {
    this.getCashiers();
  }

  getCashiers() {
    this.transactions.getAllCashiers()
        .subscribe(
          res => {
            this.cashiers = res;
        }, err => {
          console.log(err);
        });
  }

  getTransactions(id: number) {
    this.transactions.getTransactionsForCashier(id)
        .subscribe(
          res => {
            this.allTransactions = res;
          },
          err => {
            console.log(err);
          }
        );
  }

}
