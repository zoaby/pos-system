import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TransactionsService } from '../services/transactions/transactions.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  model = {
    cashierID: 0,
    cost: 0,
    paid: 0,
    change: 0
  };

  done = false;
  constructor(private transactions: TransactionsService) { }

  ngOnInit() {
  }

  onCalculate(form: NgForm) {
    this.model.cashierID = this.getCurrentCashier().cashierid;
    this.model.change = form.value.paid - form.value.cost;
    this.model.cost = form.value.cost;
    this.model.paid = form.value.paid;
  }


  // get the current cashier from the local storage;
  getCurrentCashier() {
    const cashier = localStorage.getItem('cashier');
    return JSON.parse(cashier);
  }


  // save the transaction;
  onConfirm(form: NgForm) {
    this.transactions.confirmTransaction(this.model)
        .subscribe(
          res => {
            this.done = true;
            setTimeout(() => {
              this.done = false;
              form.resetForm();
              this.model.change = 0;
            }, 4000);
          }, err => {
            console.log(err);
          }
        );
  }
}
