import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private router: Router, private http: HttpClient) { }

  getAllCashiers() {
    return this.http.get('/cashiers');
  }

  getTransactionsForCashier(id: number) {
    return this.http.get(`/transactions/${ id }`);
  }

  confirmTransaction(transaction) {
    return this.http.post('/transactions/add', transaction);
  }
}
