export interface Cashier {
    firstname: string;
    lastname: string;
    email: string;
}
