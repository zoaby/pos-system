import { Injectable } from '@angular/core';
import { Cashier } from './cashier';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  registerCashier(cashierInfo: Cashier) {
    return this.http.post('/cashiers/add', cashierInfo);
  }

  loginCashier(email: string) {
    return this.http.post('/cashiers/login', { email: email });
  }




}
