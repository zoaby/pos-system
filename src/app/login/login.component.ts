import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {}

  onLogin(form: NgForm) {
    if (form.value.email === '') {
      return;
    }
    this.authService.loginCashier(form.value.email)
        .subscribe(res => {
            localStorage.setItem('cashier', JSON.stringify(res[0]));
            this.router.navigate(['/add']);
          }, err => {
            console.log(err);
          });
  }

}

