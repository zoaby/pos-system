const { Client } = require('pg')
const db = require('../config/database');

const connectionString = db.databaseURI;


// postgresql database client config;
const client = new Client({
  connectionString: connectionString,
})

client.connect()


// @route      GET /transactions
// @desc       retrieve all the transactions;
module.exports.getTransactions = (req, res) => {
  
  const query = `SELECT * FROM Transaction;`
  client.query(query, (err, data) => {
      if (err) {
          console.log(err);
      } else {
          res.send(data.rows);
      }
  })
};





// @route      GET /transactions/:id
// @desc       retrieve all the transactions for specifec user;
module.exports.getTransactionsForCashier = (req, res) => {
  const id = req.params.id;

  const query = `SELECT * FROM Transaction WHERE CashierID = $1;`
  client.query(query, [id], (err, data) => {
      if (err) {
          console.log(err);
      } else {
          res.send(data.rows);
      }
  })
};



// @route      POST /transactions/add
// @desc       save new transaction;
module.exports.createNewTransaction = (req, res) => {
  const cost = req.body.cost;
  const paid = req.body.paid;
  const change = req.body.change;
  const cashierID = req.body.cashierID;

  const query = `INSERT INTO Transaction (cost, paid, change, cashierID) 
                    VALUES ($1, $2, $3, $4);
                `
  client.query(query, [cost, paid, change, cashierID], (err, data) => {
      if (err) {
          console.log(err);
      } else {
          res.send(data);
      }
  })
};

