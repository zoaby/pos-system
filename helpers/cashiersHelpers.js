const { Client } = require('pg')
const db = require('../config/database');
const connectionString = db.databaseURI;


// postgresql database client config;
const client = new Client({
  connectionString: connectionString,
})

client.connect()


// @route       GET /cashiers;
// @desc        retrieve all the cashiers in the database;
module.exports.getCashiers = (req, res) => {
    const query = `SELECT * FROM Cashier;`
    client.query(query, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.send(data.rows);
        }

    })
};


// @route      GET /cashiers/:id;
// @desc       retrieve one cashier by his id;
module.exports.getCashierById = (req, res) => {
    const id = req.params.id;
    const query = `SELECT * FROM Cashier WHERE CashierID = ${id};`
    client.query(query, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.send(data.rows);
        }

    })
};



// @route      GET /cashiers/:email;
// @desc       retrieve one cashier by his email;
module.exports.getCashierByEmail = (req, res) => {
    const email = req.body.email;
    const query = `SELECT * FROM Cashier WHERE Email = $1;`
    client.query(query, [email], (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.send(data.rows);
        }

    })
};

// @route     POST /cashiers/add
// @desc      create new cashier;
module.exports.createNewCashier = (req, res) => {
    const email = req.body.email;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;

    const query = `INSERT INTO cashier (firstname, lastname, email)
	VALUES ($1, $2, $3);`
    
    client.query(query, [firstname, lastname, email], (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.send(data);
        }

    })
};



// @route     DELETE /cashiers/:id
// @desc      delete cashier records;
module.exports.deleteCashier = (req, res) => {
    const id = req.params.id;

    const query = `DELETE FROM Cashier
                WHERE cashierid = $1;`
    
    client.query(query, [id], (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.send(data);
        }

    })
};

